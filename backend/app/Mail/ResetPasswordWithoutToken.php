<?php

namespace App\Mail;

use App\Entity\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ResetPasswordWithoutToken extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;
    
    protected $user;
    public $subject;
    protected $link;

    /**
     * @return mixed
     */

    public function __construct(User $user, $link)
    {
        $this->user = $user;
        $this->link = $link;
        $this->subject = 'Reset password to Laravel Mailable';

    }
    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return mixed
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
//        return $this->subject('Welcome!')->view('emails.welcome');

        return $this->from('laravel@example.com', 'Mailtrap')
            ->subject($this->subject)
            ->markdown('emails.showResetForm',
            [
                'first_name'=> $this->getUser()->first_name,
                'email'=> $this->getUser()->email,
                'token'=> $this->getUser()->token,
                'link'=> $this->getLink(),
            ]);
//            ->with(
//                'user', $this->user
//            );
    }
}
