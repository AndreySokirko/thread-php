<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use stdClass;

class ResetEmailTest extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;
    private $token;
    private $email;
    private $name;
    private $link;
    /**
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * @var stdClass
     */
    private $instance;

    public function __construct(string $first_name, string $email, string $token)
    {
        //
        $this->token = $token;
        $this->email = $email;
        $this->name = $first_name;
        $this->link = config('base_url') . 'passwords/reset/' . $token . '?email=' . urlencode($email);

//        $this->instance = $instance;
    }
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
//        return $this->subject('Welcome!')->view('emails.welcome');
        return $this->from('laravel@example.com', 'Mailtrap')
            ->subject('Mailtrap Confirmation')
            ->markdown('emails.passwords.reset' )
            ->with([
                'link' => $this->getLink(),
                'name' => $this->getName(),
                'email' => $this->getEmail(),
            ]);
    }
}
