<?php

namespace App\Mail\Auth;


use App\Entity\User;
use Http\Client\Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class VerifyMail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;
    protected $user;
    public $subject;
    /**
     * VerifyMail constructor.
     * @param User $user
     */
    public function __construct($user, $subject)
    {
        $this->user = $user;
        $this->subject = $subject;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }
    public function getSubject()
    {
        return $this->subject;
    }

    public function build()
    {
        return $this->subject($this->getSubject())
            ->view('emails.sample_email', [
                'user' => $this->getUser()
            ]);
    }
}