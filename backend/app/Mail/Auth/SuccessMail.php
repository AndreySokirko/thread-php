<?php

namespace App\Mail\Auth;


use App\Entity\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SuccessMail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    protected $user;

    /**
     * VerifyMail constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;

    }
    /**
     * @return User
     */
    private function getUser()
    {
        return $this->user;
    }

    public function build()
    {
        return $this
            ->subject('Signup Confirmation')
            ->markdown('emails.auth.confirm')
            ->with([
                'user'=> $this->getUser()
            ]);
    }
}