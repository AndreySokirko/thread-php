<?php

namespace App\Mail;


use App\Entity\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class LikedMail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    protected $like;
    protected $user;
    protected $nameUserCommentedLiked;
    protected $nameUserCommented;
    /**
     * VerifyMail constructor.
     * @param User $user
     */
    public function __construct($userWhoComment, $userWhoLikedComment)
    {
        $user = new User();
        $this->nameUserCommentedLiked = $userWhoLikedComment;
        $this->nameUserCommented = $userWhoComment;

    }

    /**
     * @return User
     */
    private function getLike()
    {
        return $this->like;
    }
   private function getWhoCommentedName()
    {
        return $this->nameUserCommented;
    }
    private function getWhoLikedName()
    {
        return $this->nameUserCommentedLiked;

    }
    public function build()
    {
        return $this
            ->subject('Signup Confirmation')
            ->markdown('emails.likedComment', ['nameUserCommented' => $this->getWhoCommentedName(), 'nameUserCommentedLiked' =>$this->getWhoLikedName()]);
    }
}