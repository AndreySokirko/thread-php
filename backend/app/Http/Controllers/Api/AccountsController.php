<?php

namespace App\Http\Controllers\Api;

use App\Entity\User;
use App\Http\Controllers\ApiController;
use App\Mail\Auth\SuccessMail;
use App\Mail\Auth\VerifyMail;
use App\Mail\ResetPasswordWithoutToken;
use Auth;
use DB;
use Hash;
use Http\Client\Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;
use Mail;
use Schema;
use Validator;

class AccountsController extends ApiController
{
    //
    public function validatePasswordRequest(Request $request)
    {
        //Create Password Reset Token
        // checck for image_url

        $user  = new User();
        $tableName = $user->getTable();
        $listColumns = Schema::getColumnListing($tableName);
        if (!in_array('token', $listColumns))
        {
            DB::statement( "ALTER TABLE $tableName ADD token VARCHAR(255)" );
        }
        // end check image_url
        //Get the token just created above

    if (!User::where('email',$request->email)->first())
        {
            $data = ['message' => "No find email! $request->email"];
            return $this->createSuccessResponse($data);
        }

        $userToken = Str::random(60);
        User::where('email',$request->email)
            ->update(['token' => $userToken]);
        $user = User::where('email',$request->email)->first();

        $subject = 'Welcome to Laravel Mailable';
        $link = $request->link.'/auth/ResetPasswordWithToken?email='.$request->email.'&token='.$userToken;
        try {
            Mail::to($request->email)->send(new ResetPasswordWithoutToken($user, $link));
            $data = ['message' => "Check email"];
            return $this->createSuccessResponse($data);

            } catch(\Exception $e) {
            throw new \Exception('A Network Error occurred. Please try again.');
        }
    }


    public function resetPassword(Request $request)
    {
        //Validate input
        $messages = [
            'email'    => 'The email is not correct',
            'password'    => 'The password is not correct',
            'token' => 'The token is not correct',
        ];
        $rules = [
            'email' => 'required|email|exists:users,email',
            'password' => 'required|confirmed|min:1',
            'password_confirmation'=>'sometimes|required_with:password',
            'token' => 'required' ];

        $validator = Validator::make($request->all(),$rules , $messages);
        //check if payload is valid before moving on
        if ($validator->fails()) {
            $message = $validator->errors();
            return $this->createSuccessResponse($message->toArray());
        }

        $password = $request->password;
        // Validate the token
        $user = User::where('token',$request->token)->first();
        if ($user == null) {
            return $this->createErrorResponse("Token not valid",301);
        }

        User::where('token',$request->token)
            ->update(['password' => Hash::make($password)]);
        //Delete the token
//        User::where('token',$request->token)
//            ->update(['token' => '']);
        //Send Email Reset Success Email

        try {

            Mail::to($user->email)->send(new SuccessMail($user));
            $data = ['message' => "Password changed"];
            return $this->createSuccessResponse($data);

        } catch(\Exception $e) {
            throw new \Exception('A Network Error occurred. Please try again.');

        }
    }

/*    public function showResetForm(Request $request)
    {
        //Validate input
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|exists:users,email',
            'token' => 'required' ]);

        return view('auth.passwords.reset')->with(
            ['token' => $request->token, 'email' => $request->email]
        );
    } */

}
