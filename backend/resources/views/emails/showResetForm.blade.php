@component('mail::message')
# Email Confirmation
Dier, {{ $first_name }}
Please refer to the following link:
or simple delte the message
@component('mail::button', ['url' => $link, 'token' => $token, 'email'=>$email])
Reset Email
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent