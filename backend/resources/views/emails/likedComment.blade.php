@component('mail::message')
  Hello **{{ $nameUserCommented }}**,  {{-- use double space for line break --}}
  Thank you for choosing Mailtrap!

  Click below to start working right now
@component('mail::panel')
  Your comment liked by {{ $nameUserCommentedLiked }}
@endcomponent
  Sincerely,
  Mailtrap team Andrey.
@endcomponent
